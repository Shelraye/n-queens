
# takes the size of n by n board and returns 
# a list of the rows of the queen in each column, 
# if a solution exists,
# else return none 

# There are three main parts to this problem:

# How to model the chessboard
# How to determine if a position is "safe" for a given column
# Figuring out the loops



global N
N = 8
 
def printSolution(board):
    for i in range(N):
        for j in range(N):
            print (board[i][j],end=' ')
        print()


def isSafe(board, row, col):
 
    for i in range(col):
        if board[row][i] == 1:
            return False
 

    for i, j in zip(range(row, -1, -1), range(col, -1, -1)):
        if board[i][j] == 1:
            return False
 

    for i, j in zip(range(row, N, 1), range(col, -1, -1)):
        if board[i][j] == 1:
            return False
 
    return True


def find_positions(board, col):
    if col >= N:
        return True
 
    for i in range(N):
        if isSafe(board, i, col):
            board[i][col] = 1
            if find_positions(board, col + 1) == True:
                return True
            board[i][col] = 0
    return False


def solveNQ():
    board = [ [0, 0, 0, 0],
              [0, 0, 0, 0],
              [0, 0, 0, 0],
              [0, 0, 0, 0]
             ]
 
    if find_positions(board, 0) == False:
        print ("Solution does not exist")
        return False
 
    printSolution(board)
    return True